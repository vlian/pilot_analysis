#include "TFile.h"
#include "TString.h"
#include "TNtuple.h"
#include <algorithm>
#include <vector>
#include <stdlib.h>
#include <iostream>
#include <math.h>
vector<vector<double>> rectangleCut(vector<vector<double>> arrayin,double xmin,double xmax,double ymin,double ymax){
	vector<vector<double>> cutarray;
	int asize = arrayin.size();
	for(int i=0;i<asize;i++){
		if(arrayin[i][3]<xmin)continue;
		if(arrayin[i][3]>xmax)continue;
		if(arrayin[i][4]<ymin)continue;
		if(arrayin[i][4]>ymax)continue;
		cutarray.push_back(arrayin[i]);
	}
	return cutarray;
}

vector<vector<double>> pyramidCut(vector<vector<double>> arrayin,vector<double> seed,double alphacut){
	vector<vector<double>> cutarray;
//	cout << "seed = " << seed[0] << " " << seed[1] << " " << seed[2] << " " << seed[3] << " " << seed[4] << " " << seed[5] << " " << seed[6] << " " << seed[7] << " " << seed[8] << " " << seed[9] << endl;
	double dz = arrayin[0][5]-seed[5];
//	cout << "dz = " << dz << endl;
	double txmax = tan(atan(seed[6])+alphacut);
	double txmin = tan(atan(seed[6])-alphacut);
	double tymax = tan(atan(seed[7])+alphacut);
	double tymin = tan(atan(seed[7])-alphacut);
	double xmax = seed[3]+dz*txmax;
	double xmin = seed[3]+dz*txmin;
	double ymax = seed[4]+dz*tymax;
	double ymin = seed[4]+dz*tymin;
//	cout << "limits " << xmin << " " << xmax << " " << ymin << " " << ymax << endl;
	int asize = arrayin.size();
	for(int i=0;i<asize;i++){
		if(arrayin[i][3]<xmin)continue;
		if(arrayin[i][3]>xmax)continue;
		if(arrayin[i][4]<ymin)continue;
		if(arrayin[i][4]>ymax)continue;
		cutarray.push_back(arrayin[i]);
	}
	return cutarray;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vector<vector<double>> extrap(vector<vector<double>> arrayin,vector<double> bt,double drcut,double dthetacut){
	vector<vector<double>> cutarray;
	double dz = arrayin[0][5]-bt[5];
	double xbt = bt[3];
	double ybt = bt[4];
	double txbt = bt[6];
	double tybt = bt[7];
	double xextrap = xbt+dz*txbt;
	double yextrap = ybt+dz*tybt;
//	cout << "extrap = " << xextrap << " " << yextrap << endl;
	int asize = arrayin.size();
	double x,y,tx,ty,dtheta;
	for(int i=0;i<asize;i++){
		x = arrayin[i][3];
		y = arrayin[i][4];
		if( sqrt((x-xextrap)*(x-xextrap)+(y-yextrap)*(y-yextrap))>drcut)continue;
		tx = arrayin[i][6];
		ty = arrayin[i][7];
		dtheta = acos((tx*txbt+ty*tybt+1)/sqrt((tx*tx+ty*ty+1)*(txbt*txbt+tybt*tybt+1)));
		if(dtheta>dthetacut)continue;
		cutarray.push_back(arrayin[i]);
	}
	return cutarray;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/*
vector<vector<vector<double>>> clusteringold(vector<double> seed,vector<vector<vector<double>>> btarray,int depth,double alphacut,double drcut,double dthetacut){
	vector<vector<vector<double>>> cluster;
	vector<double> bt;
	vector<vector<double>> clslyr, clslyrforextrap;
	vector<vector<double>> lyrarray = btarray[1]; // get plate 91
	vector<vector<double>> pyrarray = pyramidCut(lyrarray,seed,alphacut);
	vector<vector<double>> extarray = extrap(pyrarray,seed,drcut,dthetacut);
//	cout << "clslyr.size() = " << clslyr.size() << endl;
	clslyr.insert(clslyr.end(),extarray.begin(),extarray.end());
	cluster.push_back(clslyr);
	clslyrforextrap = clslyr;
	int clslyrforextrapsize = clslyrforextrap.size();
//	cout << "clslyr.size() = " << clslyrsize << endl;	
	clslyr.clear();
	for(int lyr=1;lyr<5;lyr++){
		cout << "lyr " << lyr << " clslyr.size() = " << clslyrforextrapsize << endl;
		pyrarray = pyramidCut(lyrarray,seed,alphacut);
		for(int i=0;i<clslyrforextrapsize;i++){
			bt = clslyrforextrap[i]; // Get the ith row of the last clslyr array
			extarray = extrap(pyrarray,bt,drcut,dthetacut);
			clslyr.insert(clslyr.end(),extarray.begin(),extarray.end());
		}
		cluster.push_back(clslyr);
		clslyrforextrap = clslyr;
	}
	return cluster;
}*/
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vector<vector<vector<double>>> clustering(vector<double> seed,vector<vector<vector<double>>> btarray,int depth,double alphacut,double drcut,double dthetacut){
	vector<double> bt; // The BT used for extrapolation
	vector<vector<double>> prevclslyr,clslyr; // The previous layer of the cluster, and new layer to be added to it
	vector<vector<double>> lyrarray,pyrarray,extarray; // Get one layer from the BT array, apply pyramid cut, and do BT extrapolation
	vector<vector<vector<double>>> cluster; // The final cluster to be returned
	prevclslyr.push_back(seed); // At the start of clustering there is only one BT in the prevclslyr
	int clslyrsize = prevclslyr.size();
	for(int lyr=0;lyr<depth;lyr++){ // Loop over all layers
		cout << "prevclslyr.size() = " << prevclslyr.size() << endl;
		lyrarray = btarray[lyr+1]; // Select one plate from the input array
		pyrarray = pyramidCut(lyrarray,seed,alphacut); // Apply the pyramid cut, done once per layer
		for(int i=0;i<prevclslyr.size();i++){ // Loop over BTs in the previous cluster layer
			bt = prevclslyr[i]; // Get the ith BT in prevclslyr
			extarray = extrap(pyrarray,bt,drcut,dthetacut);
			for(int j=0;j<extarray.size();j++){
				clslyr.push_back(extarray[j]);
			}// TODO: Remove the duplicates from clslyr
		}
		cluster.push_back(clslyr);
		prevclslyr = clslyr;
		clslyr.clear();
	}
	return cluster;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


void clstest(TString input){
	TFile *f = TFile::Open(input);
	vector<vector<double>> *arr90,*arr91,*arr92,*arr93,*arr94,*arr95,*arr96,*arr97,*arr98,*arr99,*arr100,*arr101,*arr102,*arr103,*arr104,*arr105,*arr106,*arr107,*arr108,*arr109,*arr110,*arr111,*arr112,*arr113,*arr114,*arr115,*arr116,*arr117,*arr118;
	TString name;
	f->GetObject("arr90",arr90);
	f->GetObject("arr91",arr91);
	f->GetObject("arr92",arr92);
	f->GetObject("arr93",arr93);
	f->GetObject("arr94",arr94);
	f->GetObject("arr95",arr95);
	f->GetObject("arr96",arr96);
	f->GetObject("arr97",arr97);
	f->GetObject("arr98",arr98);
	f->GetObject("arr99",arr99);
	f->GetObject("arr100",arr100);
	f->GetObject("arr101",arr101);
	f->GetObject("arr102",arr102);
	f->GetObject("arr103",arr103);
	f->GetObject("arr104",arr104);
	f->GetObject("arr105",arr105);
	f->GetObject("arr106",arr106);
	f->GetObject("arr107",arr107);
	f->GetObject("arr108",arr108);
	f->GetObject("arr109",arr109);
	f->GetObject("arr110",arr110);
	f->GetObject("arr111",arr111);
	f->GetObject("arr112",arr112);
	f->GetObject("arr113",arr113);
	f->GetObject("arr114",arr114);
	f->GetObject("arr115",arr115);
	f->GetObject("arr116",arr116);
	f->GetObject("arr117",arr117);
	f->GetObject("arr118",arr118);	
	vector<vector<vector<double>>> dataarray;
	dataarray.push_back(*arr90);
	dataarray.push_back(*arr91);
	dataarray.push_back(*arr92);
	dataarray.push_back(*arr93);
	dataarray.push_back(*arr94);
	dataarray.push_back(*arr95);
	dataarray.push_back(*arr96);
	dataarray.push_back(*arr97);
	dataarray.push_back(*arr98);
	dataarray.push_back(*arr99);
	dataarray.push_back(*arr100);
	dataarray.push_back(*arr101);
	dataarray.push_back(*arr102);
	dataarray.push_back(*arr103);
	dataarray.push_back(*arr104);
	dataarray.push_back(*arr105);
	dataarray.push_back(*arr106);
	dataarray.push_back(*arr107);
	dataarray.push_back(*arr108);
	dataarray.push_back(*arr109);
	dataarray.push_back(*arr110);
	dataarray.push_back(*arr111);
	dataarray.push_back(*arr112);
	dataarray.push_back(*arr113);
	dataarray.push_back(*arr114);
	dataarray.push_back(*arr115);
	dataarray.push_back(*arr116);
	dataarray.push_back(*arr117);
	dataarray.push_back(*arr118);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	vector<vector<double>> lyr90array = rectangleCut(dataarray[0],44000,50000,44000,50000);
	vector<double> seed = lyr90array[25];
	cout << "seed = ";
	for(int j=0;j<10;j++)cout << seed[j] << " ";
	cout << endl;

	vector<vector<vector<double>>> clstest = clustering(seed,dataarray,29,0.03,50,0.03);
	f->Close();
}
