#include "TFile.h"
#include "TNtuple.h"
#include<stdio.h>
#include<EdbDataSet.h>
#include<EdbPattern.h>

int main(int argc, char *argv[]){
	if(argc<=1){
		printf("Usage: myanalysis lnk.def\n");
		return 1;
	}
	
	// Declear the EdbDataProc object with the definition file "lnk.def"
	EdbDataProc *dproc = new EdbDataProc(argv[1]);

	// Read track data (data type=100 means read only the reconstructed tracks from linked_tracks.root)
	dproc->InitVolume( 0);
	
	// Get EdbPVRec object
	EdbPVRec *pvr = dproc->PVR();
	
	// Loop over the patterns = plates
	int npat = pvr->Npatterns();

	TFile *f = new TFile(argv[2],"RECREATE");
	TNtuple *nt = new TNtuple("ntuple","","id:plate:pid:x:y:z:tx:ty:w:chi2");
	for(int ipat=0; ipat<npat; ipat++){
		EdbPattern *pat = pvr->GetPattern(ipat);
		
		// access to each basetrack
		// number of segments = pat->N();
		int nseg = pat->N();
		for(int iseg=0; iseg<nseg; iseg++){
			EdbSegP *s = pat->GetSegment(iseg);
//			printf("%8d %3d %3d %8.1f %8.1f %8.1f %7.4f %7.4f\n", s->ID(), s->Plate(), s->PID(), s->X(), s->Y(), s->Z(), s->TX(), s->TY());
//			std::cout << s->ID() << " " << s->X() << " " << s->Y() << std::endl;
			nt->Fill(s->ID(),s->Plate(),s->PID(),s->X(),s->Y(),s->Z(),s->TX(),s->TY(),s->W(),s->Chi2());
		}
	}
	nt->Write();
	f->Close();
	
	return 0;
}
